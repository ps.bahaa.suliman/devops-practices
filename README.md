# DevOps Team Assignment

## Compilation
To compile the code, run the following command:

```mvn clean package```

## Running the application
To run the application locally using mySql database, use the following:

```java -jar assignment-0.0.1-SNAPSHOT.jar```

This will run the application on port 8090. To change that, use the following:

```java -jar -Dserver.port=8070 assignment-0.0.1-SNAPSHOT.jar```

To use H2 embedded in memory database, please use the following command:

```java -jar -Dspring.profiles.active=h2 assignment-0.0.1-SNAPSHOT.jar```

```java -jar -Dserver.port=8070 -Dspring.profiles.active=h2 assignment-0.0.1-SNAPSHOT.jar```

## Changing database configuration
The default database connection information is as following:
database: assignment
username: root
password: P@ssw0rd

To change any of the previous ones you can change the following in the application.yaml:
spring:
  datasource:
    url: jdbc:mysql://127.0.0.1/assignment?allowPublicKeyRetrieval=true
    username: root
    password: P@ssw0rd

or, you can pass them to the application using java properties style (-D):

```java -jar -Dserver.port=8070 -Dspring.datasource.username=newuser  -Dspring.datasource.password=newpassword assignment-0.0.1-SNAPSHOT.jar```

## Exposed end points
The application exposes the following endpoints:

### /persons
This end point follows standard REST convention, in which, you can do the following:

 |HTTP Verb|Endpoint URL|Action|
 |---------|------------|------|
 |POST|/persons/|Create new person|
 |PUT|/persons/:id|Update existing person|
 |DELETE|/persons/:id|Delete existing person|
 |GET|/persons/|List persons|
 |GET|/persons/:id|Get specific person|
 
 For the POST and PUT, you have to provide JSON payload (request body) in the following format:
 ```$xslt
{
    "name": "a name",
    "age": 23
```
Also, make sure to include the following HTTP header:
```$xslt
Content-Type: application/json
```

When creating a new person, the HTTP response will include "Location" header which indicate the ID of the newly create person. You can use this ID later to update, delete or retrieve that person.
The list persons endpoint take two optional parameter: page and size, so if you have too many persons, and you want to list them in pages, then use the following url:

```http://localhost:8090/persons/?page=0&size=10```

This will list the first 10 records from the database. The default value for page is 0 and for size is 10.

### /actuator
This endpoint provides some health checking capabilities include:

- /health: Return HTTP 200 with the the word "Up" in response if the application is up and running.
- /prometheus: provide prometheus endpoint in order to allow prometheus to monitor the application

### /Maven Version Update 
```$xslt

mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$(./versionUpdate.sh) 
```
### /Dockerfile

#### Image Build
```$xslt
- ver=$(./versionUpdate.sh)

- docker build -t bahaa96/bahaa:$ver .

- docker run -p 8080:8080 -t bahaa96/bahaa:tag
```
#### Image Push
```$xslt
- docker login -u "username" -p "password" docker.io
- docker push bahaa96/bahaa:$ver
```
#### Image Pull
```$xslt
- docker pull bahaa96/bahaa:latest
 ```
### /Docker Compose
```$xslt
. For Run Compose file :
- docker-compose up
. For Stop Compose file :
- docker-compose down
```
### /Docker Swarm
```$xslt
. New Swarm :
- docker swarm init 
. Node Information :
- docker node ls 
```

#### /Docker Stack Deploy
```$xslt
docker stack deploy --compose-file docker-compose.yml devops-practices
```
#### / verify Stack services
```$xslt
- docker stack services devops-practices 
. Output : ID                  NAME                                   MODE                REPLICAS            IMAGE                  PORTS
           b56fmak8wtrx        devops-practices_Application-service   replicated          1/1                 bahaa96/bahaa:latest   *:8080->8080/tcp
           hwpbhwkkrumm        devops-practices_mysql-service         replicated          1/1                 mysql:8.0.19           *:3306->3306/tcp

```
### / Kubernetes

##### / Health Check, pods config with readiness,
Restart Pod after 3 seconds of failure:
```$xslt
        readinessProbe:
          httpGet:
            path: /actuator/health
            port: 8080
          initialDelaySeconds: 3
          periodSeconds: 3
```
#### / config secret pod :
Base64 ENCODE
```$xslt
echo -n 'secret' | base64
```
Base64 DECODE
```$xslt
echo YXNzaWdubWVudA== | base64 --decode
```
Pod information: 
```$xslt
apiVersion: v1
kind: Secret
metadata:
  name: mysql-secrets
  namespace: devops
type: Opaque
data:
  ROOT_PASSWORD: UEBzc3cwcmQ=
  DATASOURCE_PASSWORD: UEBzc3cwcmQ=
```

kubectl get all -namespaces -n devops  
```$xslt                  
pod/mysql-855d46b9fb-rq5m2    1/1     Running   0          118s
pod/devops-677549bd96-7z6bw   1/1     Running   0          2m5s

NAME             TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/devops   ClusterIP   10.43.34.146   <none>        8080/TCP   2m3s
service/mysql    ClusterIP   10.43.81.106   <none>        3306/TCP   112s

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/mysql    1/1     1            1           118s
deployment.apps/devops   1/1     1            1           2m5s

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/mysql-855d46b9fb    1         1         1       118s
replicaset.apps/devops-677549bd96   1         1         1       2m5s
```
### / ConfigMap
```$xslt 
- kubectl describe configmaps dbcheck -n devops

Name:         dbcheck
Namespace:    devops
Labels:       <none>
Annotations:  
Data
====
db.sh:
----
#!/bin/sh
until nslookup mysql;
do echo waiting for mysql;
sleep 2;
done;

```
### / kubernetes PersistentVolume & PersistentVolumeClaim
```$xslt
- kubectl get pv -n devops

NAME              CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                   STORAGECLASS   REASON   AGE
mysql-pv-volume   20Gi       RWO            Retain           Bound    devops/mysql-pv-claim   manual                  5m33s

```

### / Destroy kubernetes namespace
```$xslt 
kubectl delete namespaces devops
```

### /Helm Chart

-To Check helm list 
```$xslt 
helm list --all
```
-To Install helm
```$xslt
- This command with custom namespace ( devops )

helm install --name devops --namespace devops /home/bahaa/devops-practices/Helm/devops
```
-To Delete helm Chart
```$xslt
helm delete --purge devops
```
### /Nginx-ingress

- self-signed certificates
```$xslt
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ops.crt -keyout ops.key

echo "$(cat nops.crt)" | base64 -w 0 
echo "$(cat nops.key)" | base64 -w 0 
```
On Chart/templetes :
```
apiVersion: v1
kind: Secret
metadata:
  name: bahaa-ops-com
  namespace: devops
data:
  tls.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUdBekNDQSt1Z0F3SUJBZ0lVWHh4dXp2d3daRldRVm5OMlFMR0JabWk1aWdZd0RRWUpLb1pJaHZjTkFRRUwKQlFBd2daQXhDekFKQmdOVkJBWVRBa3BQTVE0d0RBWURWUVFJREFWQmJXMWhiakVPTUF3R0ExVUVCd3dGUVcxdApZVzR4RWpBUUJnTlZCQW9NQ1dKaGFHRmhkR1Z6ZERFU01CQUdBMVVFQ3d3SlltRm9ZV0YwWlhOME1SWXdGQVlEClZRUUREQTFpWVdoaFlTNXZjSE11WTI5dE1TRXdId1lKS29aSWh2Y05BUWtCRmhKaVlXaGhZVGsxTmtCbmJXRnAKYkM1amIyMHdIaGNOTWpBd056RXhNVGsxTWpVMVdoY05NakV3TnpFeE1UazFNalUxV2pDQmtERUxNQWtHQTFVRQpCaE1DU2s4eERqQU1CZ05WQkFnTUJVRnRiV0Z1TVE0d0RBWURWUVFIREFWQmJXMWhiakVTTUJBR0ExVUVDZ3dKClltRm9ZV0YwWlhOME1SSXdFQVlEVlFRTERBbGlZV2hoWVhSbGMzUXhGakFVQmdOVkJBTU1EV0poYUdGaExtOXcKY3k1amIyMHhJVEFmQmdrcWhraUc5dzBCQ1FFV0VtSmhhR0ZoT1RVMlFHZHRZV2xzTG1OdmJUQ0NBaUl3RFFZSgpLb1pJaHZjTkFRRUJCUUFEZ2dJUEFEQ0NBZ29DZ2dJQkFOcFVEVzJ0b1BwaEErZndiM3Q2UE1wWmlUSVZMVjhGCkVhbmZFN3ZSdStleGZzS1VGUnhMUlJQbW42dS9YVm1HaEVjZldnVTNmbkJibEZ4OFdvRkR1cmpIenY5N2pCaHUKekd6WDRhLzFpV0hoVkpDS09KbWZiZkVDMGllY0ppa2lrS0Z3UDY2T3ZlN3pzMnpMTGlSeW5Bakloamp0TmwrVwoxdWh2dXY3L1ZPWFNwNkJWZGxIOU9XRnJPS1l4THdiQUlVb0FUQTRuTmVTRklhcno1K2x1SmliVzg3VVRFRUxxCnVNYzNTTXczNG5xa08vOXVJZWdOU3pBVFUwUHAvZHpGQXJaWG5HcnQ3Ull3UUxScVptMGF4bFJ1QWtLRjVncloKdWNJejIrTWtsK1FJMjcxbzRTUFBqNktZVG9RVWhUclZMcnJBbnpHTVBUWWhpR2tPeDYrQXVEWEMrZWR4eWpLOApFNi9HV1F2ZWxobzhzbENvSmVUR1IvMnhUTzBSUnUyYm5mQllXa1BSWDljTS9PTFdVRUV1M000K1UvL242NjlJCnYxK0J3WnRHTmRDdWdQZis5am01RStKTjZiZk1lSTF2SHNFdlIxTkVFc2xraldTTlJyWGpkKzVhRmRmWWpWSlUKdHk3VnZhMnc3QWUzSllxK2dIMWh2QnIwQ1JHN1pPcGY3VXlkcWZtYi91Mmw3WE40dG0rWGVsU3F1Z2k4U1d4RwpsSVVlMy9TZWc3am1LbEp5THpmSG43OXp6alBWNStYU29oYjFGaEdUaXFLb04ycGV2RzNiczZtRkFJYVpEclJ1Cit4eGlXTjBhSExNS2JLaHo0ditGWVpPdkVYeWdBN1REMDdFYUZ6VjgzUDh5TTEyQ1kvR1NtZnU3ZFhXdm1KbFQKV1ZFZ29OZzBod3dqQWdNQkFBR2pVekJSTUIwR0ExVWREZ1FXQkJTbUpIK0gzdnEwMkFXKyt6bFluczI5bERDYQpMREFmQmdOVkhTTUVHREFXZ0JTbUpIK0gzdnEwMkFXKyt6bFluczI5bERDYUxEQVBCZ05WSFJNQkFmOEVCVEFECkFRSC9NQTBHQ1NxR1NJYjNEUUVCQ3dVQUE0SUNBUUFiaUU5K1l5ckpmbWlVd0xUU04wK2ZDK2VzbVJlR0JqUkoKOEovV1FadUxWZXREN0ZTem5qZUp4NGhPR3pqWHFWaVR0d0U3TFd1ZFhXM3FJY3A4NU1pZ3ZXNituVG9ISmlpbAo4Um1VanFwR2pGSnBJV1FNMDJWeDFSSHc5VmdJQkhaN2tuV2tQUE5HcW02VmxGTFFiTWlUV2pYVzZwMUJqWXdmClJRWVFZUjVxbU95RTBzVGFNR2xmYklZUlExRzJIMGNlMDVJOFlqUDZZRStrblYyZmpEeHFaNHVFbVVvb3MxbXAKbE5aTngveFdwMzlnVjJyd3RZalFmcHZZNWl6RE8zSzVlU0EwVVdISE9MTFY3S1lpcHNJRHdad21LUDg3WERpRQpBeGdqNlFpYkRESTBWRmhQOEZDRm1TRFZwSUV4ZWs0WmdHU2dZUTk2TUFlNUUvTVE4eGswcWkwNG5jazd0YWFtCnRFYzZOUmtzUHZNNHJJZ3pXZlgwT05iYjJENHREdndSbzhJUGxRdnRJMkREVkZTVVo1cFl2eXZyd3F3aEozSEQKQytpWGNESkpNcWNxNkhIYjZhamJya01obnRnL3BIZ2FRMmM1dWdiblJCOVp4V3NnNmcxRGlvdnk3bzZpNTE4bwpjQ2tzRmk3eEpTd3pEc2NILzRQaGVkamoydXl3SUlQZE9vQlpUQ3JyblkwTk1uei8yY3lYOWZpRGJYU21Ld2IvClBCK3prdTB1ZXJJVWNtbVhGUTZLcjhZUWplcjkrdGJSU3BFK0NkSE1ZUStvdzBEdmJlMnNYdDBvWFlhdDU1TUYKemwzd0ZvUGNYRDgxL09VY2JvM0FycjMrUEg5NFJwWm1uRXZGTTRxcnNlZ08wSnRnYUU2Z1cvK3h3TlFLbEFsWgp3OWxSNDhjN0l3PT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
  tls.key: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUpSUUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQ1M4d2dna3JBZ0VBQW9JQ0FRRGFWQTF0cmFENllRUG4KOEc5N2VqektXWWt5RlMxZkJSR3AzeE83MGJ2bnNYN0NsQlVjUzBVVDVwK3J2MTFaaG9SSEgxb0ZOMzV3VzVSYwpmRnFCUTdxNHg4Ny9lNHdZYnN4czErR3Y5WWxoNFZTUWlqaVpuMjN4QXRJbm5DWXBJcENoY0QrdWpyM3U4N05zCnl5NGtjcHdJeUlZNDdUWmZsdGJvYjdyKy8xVGwwcWVnVlhaUi9UbGhhemltTVM4R3dDRktBRXdPSnpYa2hTR3EKOCtmcGJpWW0xdk8xRXhCQzZyakhOMGpNTitKNnBEdi9iaUhvRFVzd0UxTkQ2ZjNjeFFLMlY1eHE3ZTBXTUVDMAphbVp0R3NaVWJnSkNoZVlLMmJuQ005dmpKSmZrQ051OWFPRWp6NCtpbUU2RUZJVTYxUzY2d0o4eGpEMDJJWWhwCkRzZXZnTGcxd3ZubmNjb3l2Qk92eGxrTDNwWWFQTEpRcUNYa3hrZjlzVXp0RVVidG01M3dXRnBEMFYvWERQemkKMWxCQkx0ek9QbFAvNSt1dlNMOWZnY0diUmpYUXJvRDMvdlk1dVJQaVRlbTN6SGlOYng3QkwwZFRSQkxKWkkxawpqVWExNDNmdVdoWFgySTFTVkxjdTFiMnRzT3dIdHlXS3ZvQjlZYndhOUFrUnUyVHFYKzFNbmFuNW0vN3RwZTF6CmVMWnZsM3BVcXJvSXZFbHNScFNGSHQvMG5vTzQ1aXBTY2k4M3g1Ky9jODR6MWVmbDBxSVc5UllSazRxaXFEZHEKWHJ4dDI3T3BoUUNHbVE2MGJ2c2NZbGpkR2h5ekNteW9jK0wvaFdHVHJ4RjhvQU8wdzlPeEdoYzFmTnovTWpOZApnbVB4a3BuN3UzVjFyNWlaVTFsUklLRFlOSWNNSXdJREFRQUJBb0lDQVFDclp3OS95cTgxd2hKcDlkWGY3N0JuCi8vRDRzNjJDQ3AyNmFocTQra2gxOW1BWTdGN0Nic2xWZlZwZllJWXQ4YkJKa2ZCL1pmakt4dXJyek9qVTBOSjMKbk00Q3Z3RFdjYTNseVhveVJTYk5CRFQ2b0xrY3loQnJCUmJ0NStZQnAxUTZtY0ExRDNWSGNtV3JoTHU5KzRoSwpVUHNuRi92M0FabklzMmJ3S0k2RE8yVkVON1FEWERTL0tZazQyQ1llTzVvbTJhTGIvYWFPdUlJZER3MURzcWtiCkZ1Z1BnVjdqMmhpa3RkOWVESVd1V04rTmdlMzJtdkVyVmhDcG9ic2Vwa0NoM0pnOVRuc3lUMEdOSW5jMU1xNy8KdTdua2RmQ0xGakdDcUduNW9sSG93WmVaMVF2anJHRU1lbHd4ejFabFFOWkY0VkZaZWNyWWcyenZaWjRaUjRuVgpuLzRQTHFHVGlGOTdCdkpneEtnSXo1TjFJVWdhVmovcG0yWnFWcGhtU1dxZmhYc1QwVCttcE02WExvcWt6MXVQCjFoVUI1ZzlEM3YyMGl0S1pTK3BYRHhGdHBKZ1ZvMXN5WmQ1ckJGVGowNnBXeUZwbS9qOUY4SDRDL3BSaEcvTjcKQTYxY2ZYZjBweFE4MkowTk1BY25MbHpWNzVmRWxWbjg3L3AzL01CUENzVk5GbTJKd0tSZEx2TWdMUzJRSWwvdgpjWVFVYVEyeTdSV3NJUU1JK1E1c1MxTW1WY1RodFh1UFpQdTY4VHRjRmRGRWN6U25lT3g3U1FjamZrWnVZTUdyCnZFOW9pa0MwUFZHRDJNbHNPQ0xveWptTGNuUFNhWmQrOUpwN29WK2NiV01pVzhZYnkrekNPVWZwYnBWVEZaSzQKc0NCOVN1M1BESng5QndGR0Uxd1hpUUtDQVFFQTdNenJ0UFpuT0xNc2Y5ZjhvRTAvb0hrdWdSTk5DT0htVERpNwpWdzliZ1dGRVFmUTJNVjV5OGQvd3d0S01sS0NZdTlrWTZCcXF1ZEpETUNEWEhldUhOQ1NYRVd5VExaT01hdHpWCkQwMVd1VXRBbk9sZTgybjNodGVRRG1IM0tldUJSd215MlpwMGdtQ25iMmExd3B0Q0txc2VWNjRCRGtaSzJxcEsKTjNWa3VQTUloUGp0eGNzeHhSSzF1akVXdVpHRVFMT2JIVCtSYkQ2RUxzeTRnc2M0UGdIWkMwNGZtYWZHV1A1bgpRVmhNdFVkVVJqQmVDMS9QRkgrWjE2VjhXUkhIT3VhbGhLQmtVMXRmVHNpNzc5U0hQRFhadFppRVdreFkraUtrCjErTDNWcWwrMFFSZ09hZCtCMjgrS3JZWUJzSzdlOVBlYnBvSWxCalc3ZTBpY2N1RUJ3S0NBUUVBN0FlNFhBWHAKMkVGK0xGdzdSRzR4alEwY1ZtSndMcGUydmZwYmNxVHVJSlRUUzNYazRoUkUzeXB3aEFBVzY2VHN1U21zOXFWSApENTRRd2dWenVXQXdsVDNjRkR2K3ZzNDlaRVNOcVlwZHo1QnR4b0t0ZHROMXRzZ0hSaGNUS01LcERmaHZiOUszCjdxVVFkQ0NzOW9uYkZLSzVjUnUzUEpNVlBEOUo5YlRYUGV2QXUxZFIrTlRzci85WFZHd2RRUjlwa092N3REdnEKSWxDcDdRb0k5Q056Zk5ranpVcUh1ZFRtanRVUzJ5SXB1dFh6UWZpQ3ZQdk5DK2JYWnVTUkxUWDhSYTFUdm8rSApFc1lLUEpTZ29lb3Y3ZnlWckZGVlRVYVlObjdoYjJXTWNsRVhqcE5VakdIcnkweG1tY2E2djJ5RjI2cFhRV3EwCmxFNEIvL1dSRmxqSUJRS0NBUUVBeG9kellnOW5NT2srK0p2N1orWDczeTFaNk10bXZYaXZITjQvYWxrc3pZYmYKdEMvcnMyNU5EaTVDNndPYzZ3ck5va2Y4RXJRd3lJUFk0cVBWMW5iZHJzQ3lpdUF0Y0h1UXNSTWQ2YXRjZHhMNQpROVNBVDdGYTNna2g5SHo3dklCK3JURWJha2FOUWJXSkN1UGdwUnlIMThBSk8xZGQrVHJEalJwU0NMYjZUL2twCi9sbTQ5eWhKUE1GWDdKYzFSelVSM1REM2Jna2FnZGJUK1d5WGtNVTNpUDBCbS9IT21vTkh6dHJUZEx4ejYxTzIKK3ZXWUV3OHJUVE5CcHNOT1FscWVYdXA2Z0h2OFFNcU5XRDhCR2F4a0NQM1kzZC9WcnVMdGdpRERkV0FMSC9RUQpwMnAweGtGL2J2bzc4NUVoQ2FIL2oyeHN3dXVQSDZMSHAwZzFwWlFMeHdLQ0FRRUF1NnRqeGgrWmtLNTZjVEVnCm1zZDNOS3poQkRxZThoMEZXMGJPSVhqTU1qSERVM080K3AyZEtVaG9VRHgxL1FXVWg2R0FlbGh1Q1Jmb1FTenUKbDgzSXMxR05STFZ6VnlsTEhRZDVELzRKbXprK1g2R1Y1UzQ3b1lxb1plMkErVjdWNWxyTDJFRFdxTFNzUVhtZApyU1Z0Z1pubEV1OHpvY0ZkR0J0R1preFUvRklRVzZVUmMyVnhrSHJaYURIU2pPNDZGTlhkdTdBTCtSN2tEYXAxCjloQ2VDZjNvL3BrdEUvQVpKaVo3S0t1Z1BJTlRPUGdzazdFdWx6RUR3amNveTZicUlFaXhsb291bnhuQ0Yrc2gKVWJ5UVFZMGRRUGNtZVJ3UmVQc25xbGcyOU1wUGpiak5lQlYvMDVTNjI3aHMwam1xc1Q5ZXc0L2haeTRqWHU1VgpYMHhXTlFLQ0FRRUExYTBPdHgyZXFQSEpzS2tpdnFnSTVBZy9GMHhQMWdvSDUzUC94b2RqVlhjSlBiUmpBYTZUClprOGNOYUhwTHZET0pvbWgzeFEvbHM4TXcybXloTUlxVGlkODRLa1RhSjlmWnNUQ082NXhqVk5IR2VWeVRVZWYKK29ReXRRdzNyeUdhY0JNYUxYbXZ5ejNmVE81dXZCVVR4ZGtGN0MybEpMMW1IYWxUeHBQZFdsZFdaalRvU3FVNgpITnp4Rlpxbk4rNC9xTTl4dHZzT0M0SUkrWm04anRGNW1qSDJDdTkwUUluc2lNeXVpYnVGM0VWVFZINitGNktQCnA4Z1pheHFmM1p0a2E1ZGdMR3loRjlQK3dVMUJJWnViV0VpS2swdFZPQjRadmx3ZlJDUXBPOURiSUUrbXJld0cKc29lNVdIaHdCTmZXcW1PTmlyUk9aZ1cxZ0dzdWNrK0gzUT09Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0K
type: kubernetes.io/tls
```
```
helm upgrade --install --wait \
--set controller.service.externalIPs={"192.168.1.55"} \
--namespace nginx-system \
nginx-ingress \
/home/bahaa/k8s-offline/master-packages/charts-setup/nginx-ingress
```

###/ Access By https: https://bahaa.ops.com




### /Applications Monitoring
##### Deploy ELK ( File in found & logs in ELK-Files)
```$xslt
- kubectl apply -f elasticsearch.yaml
- kubectl apply -f kibana-deployment.yaml
- kubectl apply -f filebeat-ds.yaml 
- kubectl apply -f logstash-deployment.yaml 
- kubectl apply -f metricbeat-ds.yaml   
```
#### Prometheus Deploy Using Helm
```$xslt
- helm install stable/prometheus-operator --name prometheus-operator --namespace monitoring

- kubectl port-forward -n monitoring prometheus-prometheus-operator-prometheus-0 9090 (( Or Access the Pod port on browser ))
```
##### Collecting application prometheus metrics 
```$xslt
http://localhost:8080/actuator/prometheus
```