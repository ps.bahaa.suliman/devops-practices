FROM openjdk:8-alpine
COPY target/assignment-*.jar /assignment.jar
CMD ["java", "-jar", "/assignment.jar"]
EXPOSE 8080
