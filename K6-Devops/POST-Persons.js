import http from "k6/http";
import { check } from "k6";
import { functionName , functionAge} from "/home/bahaa/devops-practices/K6-Devops/common.js";
export let options = {
  insecureSkipTLSVerify: true,
};


export default function () {
  const params = { headers: { "Content-Type": "application/json" , "receivedFrom": "k6testing" } };
  var name = functionName();
  var age = functionAge();
  var payload = JSON.stringify(
    {
      "name": name,
      "age": age
    }
  );
  //console.log(payload)
  var result=http.post(`${__ENV.SERVICE_URL}`, payload, params)
  //console.log(result.status)
  check(result, {
    "status is 200": (r) => r.status === 200 || r.status === 201,
  });
}