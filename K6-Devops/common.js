/*global __ENV : true  */
import { fail } from "k6";

export let options = {
  insecureSkipTLSVerify: true,
};

export function logError(res) {
  if ( typeof logError.last == 'undefined' ) logError.last = '';


  let error;
  try {
    let message = JSON.parse(res.body)['message'] || JSON.parse(res.body)['error']
    error = typeof message === 'object' ? JSON.stringify(message) : message
  } catch (e) {
    error = res.body
  }

  if (logError.last != error) {
    logError.last = error;
    console.warn(`Error detected: '${logError.last}'`);
  }
}


export function functionName(){
  return 'Bahaa-DevOps' + Math.floor(Math.random()*9399);
}

export function functionAge(){
  return Math.floor(Math.random() * 100) + 1;
}

