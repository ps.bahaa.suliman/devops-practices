#!/bin/bash
export DEVOPS_SERVICE=http://localhost:8080/persons/
export iterations=1
export USERS=1

#################################

declare -a StringArray=(
    "POST-Persons"
    )

for test in ${StringArray[@]}; do

    k6 run -u 1 -i 1 -e SERVICE_URL=http://localhost:8080/persons/ ${test}.js
done